const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp();
var firestore = admin.firestore();
const settings = {/* your settings... */ timestampsInSnapshots: true};
firestore.settings(settings);
var firebase = admin.database();

var braintree = require("braintree");
var gateway = braintree.connect({
  environment: braintree.Environment.Sandbox,
  merchantId: "tgzjdsnxds69y7xs",
  publicKey: "c86276kmk9tf9cvc",
  privateKey: "655e79b4ff32ad6d790f8e6068e11a4b"
});

/*******************************************
************ CREATE METHODS ***************
*******************************************/
gateway.config.timeout = 1000;

exports.createRestaurant = functions.https.onRequest((request, response) => {
    var body = request.body;

    // Add a new document with a generated id.
    var addDoc = firestore.collection('restaurants').add(body).then(ref => {
      console.log('Added restaurant document with ID: ', ref.id);

      // Save id in body and update the object
      body["id"] = ref.id;
      updateRestaurant(body);
      response.send({ status : 200, restaurantId : ref.id, message : "Restaurant Created"})
    });

});

exports.addCategoryToMenu = functions.https.onRequest((request, response) => {
    var categoryBody = request.body;

    if(categoryBody.restaurantId === null || categoryBody.restaurantId === "" || categoryBody.restaurantId === undefined){
        response.send({ status : 406, message : "406 Not Acceptable. Restaurant ID missing or empty."});
        return;
    }

    // Add a new menu item document with a generated id.
    var addDoc = firestore.collection('restaurants').doc(categoryBody.restaurantId)
                   .collection('menu').add(categoryBody).then(ref => {
      console.log('Added category to menu with ID: ', ref.id);

      // Save id in body and update the object
      categoryBody["id"] = ref.id;
      updateCategory(categoryBody);
      response.send({ status : 200, message : "Menu Category Added"});
    });

});

exports.addItemToMenu = functions.https.onRequest((request, response) => {
  var itemBody = request.body;

  if(itemBody.categoryId === null || itemBody.categoryId === "" || itemBody.categoryId === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. Category ID missing or empty."});
      return;
  } else if(itemBody.restaurantId === null || itemBody.restaurantId === "" || itemBody.restaurantId === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. Restaurant ID missing or empty."});
      return;
  }

  // Add a new menu item document with a generated id.
  var addDoc = firestore.collection('restaurants').doc(itemBody.restaurantId)
                  .collection('menu').doc(itemBody.categoryId)
                  .collection('items').add(itemBody).then(ref => {
    console.log('Added menu item with ID: ', ref.id);

    // Save id in body and update the object
    itemBody["id"] = ref.id;
    updateMenuItem(itemBody);
    response.send({ status : 200, message : "Menu Item Added"})
  });

});

/*********************************************************
***************** RESTURANT API METHODS ******************
**********************************************************/

exports.getRestaurant = functions.https.onRequest((req,res) => {
  let restaurantId = req.body.restaurantId
  console.log(req.body)

  if (restaurantId === null || restaurantId === "" || restaurantId === undefined) {
    console.log("Invalid Request")
    return response.send({ status : 406, message : "406 Not Acceptable. restaurantId missing or empty.", request : req.body});
  }

  return getRestaurant(restaurantId)
  .then(result => {
    return res.send({
      status : 200,
      restaurant : result
    })
  })
})

/*********************************************************
************ CUSTOMER ORDER FUNNEL METHODS ***************
**********************************************************/

// When user checks into restaurant and table
exports.checkinUserToRestaurant = functions.https.onRequest((request, response) => {
  var body = request.body;

  if(body.clientId === null || body.clientId === "" || body.clientId === undefined){
    console.error(body)
    return response.send({ status : 406, message : "406 Not Acceptable. clientId ID (clientId) missing or empty."});
  }

  var restaurantRef = firestore.collection('restaurants').doc(body.restaurantId);
  return restaurantRef
    .get()
    .then(doc => {

      if (!doc.exists) {
        console.log('No Restaurant Document Found');
        return response.send({ status : 406, message : "406 Not Acceptable:  No Restaurant Document Found", error : "No User Document Found"});
      } else {
          console.log('Restaurant Data:', doc.data());
          var restaurantInfo = {
            restaurantId: doc.data().id,
            restaurantName: doc.data().name,
            restaurantAddress: doc.data().address,
            restaurantProfilePicURL: doc.data().profilePictureUrl
          }

          return restaurantInfo
      }
    }).then(restaurantInfo => {

      var userRef = firestore.collection('users').doc(body.clientId);
      return userRef
        .get()
        .then(doc => {
          if (!doc.exists) {
            console.log('No User Document Found');
            return response.send({ status : 406, message : "406 Not Acceptable:  No User Document Found", error : "No User Document Found"});
          } else {
            console.log('User Document Data:', doc.data());
            // Create a Customer Session
            var orderSessionKey = firebase.ref().child('posts').push().key.toString();
            let timestamp = timeStamp();
            var session = {
              id : orderSessionKey,
              clientId: body.clientId,
              restaurantId: body.restaurantId,
              state: "CHECKED_IN",
              tableId: body.tableId,
              userFullName: doc.data().firstName + " " + doc.data().lastName,
              userProfilePicURL: doc.data().profilePictureURL,
              restaurantName: restaurantInfo.restaurantName,
              restaurantAddress: restaurantInfo.restaurantAddress,
              restaurantProfilePicURL: restaurantInfo.restaurantProfilePicURL,
              createdAt: timestamp,
              updatedAt: timestamp
            }

            return session
          }
      })
    }).then(session => {

      console.log("Writing to database")
      console.log(session)
      // Save Order Session in order_sessions, restaurant_sessions, and user
      let writes = []
      console.log(session.id)
      writes.push(firebase.ref('/order_sessions/' + session.id).set(session, function(error) {
        if (!isEmpty(error)) {
          // The write failed...
          console.error("Error:" + error)
          throw error
        } else {
          // Data saved successfully!
          console.log("Wrote to order sessions")
          return `Order Session ${session.id} Created`
        }
      }).catch(error => {
        console.error("Error" + error)
      }))
      writes.push(firebase.ref(`/restaurant_sessions/${session.restaurantId}/checkedIn/${session.id}`).set(session))
      writes.push(firestore.doc(`users/${session.clientId}`).update({
        currentSession : session.id, currentSessionState : session.state}))

      return Promise.all(writes).then(results => {
        return session
      })
    })
    .then(session => {
      console.log(session.id + " completed function")
      return response.send({
        status : 200,
        message : `User Checked In. Session ID: ${session.id}`,
        sessionId : session.id
      })
    })
  .catch(err => {
    console.error("Getting User Document Error:"  + err);
    response.send({ status :  500 , message : "Server Side Error",  error :  err});
  });
});


// When user finishes ordering their food
// New variables added to session object are menuItems and totalPaymentAmount
exports.requestFoodForSession = functions.https.onRequest((request, response) => {
  var body = request.body;
  console.log(body)

  // If sessionId missing then return back with error
  if(body.sessionId === null || body.sessionId === "" || body.sessionId === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. Session ID (sessionId) missing or empty."});
      return;
  }

  // Fetch the session data
  firebase.ref('/order_sessions/' + body.sessionId).once('value').then(function(snapshot) {
    var session = snapshot.val();

    if(session === null){
      response.send({ status :  500 , message : "There is no record of session with ID: " + body.sessionId});
      return;
    }

    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/checkedIn/' + session.id).remove();
    session.orders = body.orders
    // Set index of each individual orders
    var i;
    for (i = 0; i < body.orders.length; i++) {
      console.log(i)
        session.orders[i].index = i
    }
    session.totalPaymentAmount = body.totalPaymentAmount;
    session.state = "PENDING_FOOD_ORDER"

    let writes = []
    writes.push(
      // Update session in the session tree
      updateSession(session)
    );
    writes.push(
      // Update session state of user
      firestore.collection('users').doc(session.clientId).update({currentSessionState : session.state})
    );
    writes.push(
      // Add the session to "pendingFoodOrder"
      firebase.ref('/restaurant_sessions/'+session.restaurantId+'/pendingFoodOrder/' + session.id).set(session)
    );
    writes.push(
      // Add the session to "orderDelivered"
      firebase.ref('/restaurant_sessions/'+session.restaurantId+'/orderDelivered/' + session.id).set(session)
    );

    return Promise.all(writes).then(results => {
        response.send({ status : 200, message : "Food Order Requested. Session ID: " + session.id, sessionId : session.id })
    })

  });

});
/*
exports.syncWithOrderSessions = functions.database.ref('/order_sessions/{sessionId}').onWrite((change, context) => {
  let before = change.before.val()
  let session = change.after.val()
  // check for status transitions
  if (before.state === 'CHECKED_IN' && session.state === 'PENDING_FOOD_ORDER') {
    console.log("syncWithOrderSessions is removing session from checkedIn tree")
    // Remove the session from "checkedIn"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/checkedIn/' + session.id).remove();
  } else if (before.state === 'PENDING_FOOD_ORDER' && session.state === 'CHECKED_OUT') {
    console.log("syncWithOrderSessions is removing session from pendingFoodOrder tree")
    // Remove the session from "pendingFoodOrder"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/pendingFoodOrder/' + session.id).remove();

    // Update session state of user
    firestore.collection('users').doc(session.clientId).update({
      currentSessionState : session.state,
      currentSession: admin.firestore.FieldValue.delete()
    });
  }

  // update pending order object as order changes
  if (session.state === 'PENDING_FOOD_ORDER') {
    console.log("syncWithOrderSessions is updating session from pendingFoodOrder tree")

    // Update session state of user
    firestore.collection('users').doc(session.clientId).update({currentSessionState : session.state});
    // Update session to "pendingFoodOrder"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/pendingFoodOrder/' + session.id).set(session);
  }
})/
*/

// When user is given their order items
exports.userOrderDelivered = functions.https.onRequest((request, response) => {
  var body = request.body;

  // If sessionId missing then return back with error
  if(body.sessionId === null || body.sessionId === "" || body.sessionId === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. Session ID (sessionId) missing or empty."});
      return;
  }

  // Fetch the session data
  firebase.ref('/order_sessions/' + body.sessionId).once('value').then(function(snapshot) {
    var session = snapshot.val();

    if(session === null){
      response.send({ status :  500 , message : "There is no record of session with ID: " + body.sessionId});
      return;
    }

    session.state = "DELIVERED"

    // Update session in the session tree
    updateSession(session);

    // Update session state of user
    firestore.collection('users').doc(session.clientId).update({currentSessionState : "DELIVERED"});

    // Add the session to "orderDelivered"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/orderDelivered/' + session.id).set(session);

    // Remove the session from "pendingFoodOrder"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/pendingFoodOrder/' + session.id).remove();

    response.send({ status : 200, message : "Session State changed to Delivered. Session ID: " + session.id, sessionId : session.id })
  });

});

// When the user finishes and pays
exports.clientCloseOut = functions.https.onRequest((request, response) => {
  var body = request.body;

  // If sessionId missing then return back with error
  if(body.sessionId === null || body.sessionId === "" || body.sessionId === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. Session ID (sessionId) missing or empty."});
      return;
  }

  // If paymentMethodNonce missing then return back with error
  if(body.paymentMethodNonce === null || body.paymentMethodNonce === "" || body.paymentMethodNonce === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. Payment Nonce (paymentMethodNonce) missing or empty."});
      return;
  }

  // Fetch the session data
  return firebase.ref('/order_sessions/' + body.sessionId).once('value').then(function(snapshot) {
    var session = snapshot.val();
    session.closedOutById = session.clientId;
    session.closedOutUserType = "CLIENT"
    timestamp = timeStamp();
    session.closedOutAt = timestamp;
    let clientId = session.clientId
    session.state = "CHECKED_OUT"

    // Update session in the session tree
    updateSession(session);

    // Remove the session from "orderDelivered"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/orderDelivered/' + session.id).remove();
    // Remove the session from "pendingFoodOrder"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/pendingFoodOrder/' + session.id).remove();

    // Update session state of user
    firestore.collection('users').doc(session.clientId).update({currentSessionState : "CHECKED_OUT"});

    // Add session to firestore session history colleciton
    firestore.collection('restaurantSessionHistory').doc(session.restaurantId).collection("closedOutSessions").doc(session.id).set(session);
    firestore.collection('clientSessionHistory').doc(session.clientId).collection("closedOutSessions").doc(session.id).set(session);

    // Remove currentSession from user
    firestore.doc(`users/${clientId}`).update({currentSession : admin.firestore.FieldValue.delete()})
    .then(result => {
      console.log(result)
    })

    if (!isEmpty(body.paymentMethodNonce)) {
      let nonceFromTheClient = body.paymentMethodNonce
      return gateway.transaction.sale({
        amount: body.amount,
        paymentMethodNonce: nonceFromTheClient,
        options: {
          submitForSettlement: true
        }
      }, function (err, result) {
        if (result.success) {
          return response.send({
            status : 200,
            message : "Client Close Out Successful. Session ID: " + session.id,
            sessionId : session.id
          })
        } else {
          throw err
        }
      });
    }

  }).catch(reject => {
    return response.send({
      status : 400,
      message : "400 Bad Request: Could not complete" + body.sessionId,
      error : reject })
  })
});

// When the waiter/employee finishes and pays for the client
exports.businessCloseOut = functions.https.onRequest((request, response) => {
  var data = request.body.data;

  // Fetch the session data
 return firebase.ref('/order_sessions/' + data.id).once('value').then(function(snapshot) {

    let timestamp = timeStamp();
    let restaurantId = snapshot.val().restaurantId

    // Prepare Session object
    var session = snapshot.val();
    session.closedOutById = restaurantId;
    session.closedOutUserType = "RESTAURANT"
    session.closedOutAt = timestamp;
    session.state = "CHECKED_OUT"

    // Update session in the session tree
    updateSession(session);

    // Remove the session from "checkedIn" tree
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/checkedIn/' + session.id).remove();
    // Remove the session from "orderDelivered"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/orderDelivered/' + session.id).remove();
    // Remove the session from "pendingFoodOrder"
    firebase.ref('/restaurant_sessions/'+session.restaurantId+'/pendingFoodOrder/' + session.id).remove();

    // Update session state of user
    firestore.collection('users').doc(session.clientId).update({currentSessionState : "CHECKED_OUT"});

    // Add session to firestore session history colleciton
    firestore.collection('restaurantSessionHistory').doc(session.restaurantId).collection("closedOutSessions").doc(session.id).set(session);
    firestore.collection('clientSessionHistory').doc(session.clientId).collection("closedOutSessions").doc(session.id).set(session);

    // Remove currentSession from user
    return firestore.doc(`users/${session.clientId}`).update({currentSession : admin.firestore.FieldValue.delete()})
    .then(result => {
      return response.send({
        data: {
          message : "Client Close Out Successful. Session ID: " + session.id,
          sessionId : session.id
        }
      })

    })


  }).catch(reject => {
      console.log("Nah")
    return response.send({
        data:{

        },
        error: {
          status : 400,
          details : "400 Bad Request: Could not complete: sessionID:" + data.id
        }
      })

  })
});

exports.getPaymentToken = functions.https.onRequest((request, res) => {
  var body = request.body;
  let paymentToken = body.paymentToken
  // If paymentToken missing then return back with error
  if(body.paymentToken === null || body.paymentToken === "" || body.paymentToken === undefined){
      res.send({ status : 406, message : "406 Not Acceptable. Payment Token (paymentToken) missing or empty."});
      return;
  }

  return gateway.paymentMethodNonce.create(paymentToken, function(err, response) {

    if (response.success) {
      return res.send({
        status : 200,
        paymentMethodNonce : response.paymentMethodNonce.nonce
      })

    } else {
      return res.send({
        status : err.status,
        message : "Couldnt create payment nonce",
        error : err
      })
    }
  });
});

exports.chargeClient = functions.https.onRequest((request, res) => {
  var body = request.body;
  let nonceFromTheClient = body.nonceFromTheClient


    return gateway.transaction.sale({
      amount: body.amount,
      paymentMethodNonce: nonceFromTheClient,
      options: {
        submitForSettlement: true
      }
    }, function (err, result) {
      if (result.success) {
        return res.send({
          status : 200,
          message : "Client Charge Successful"
        })
      }

  })

});

exports.addReviewToRestaurant = functions.https.onRequest((request, response) => {
  var reviewBody = request.body;

  var setDoc = firestore.collection('restaurants').doc(reviewBody.restaurantId).collection('reviews').add(reviewBody).then(ref => {
    console.log('Added Restaurant Review ID: ', ref.id);

    // Save id in body and update the object
    reviewBody["id"] = ref.id;
    firestore.collection('restaurants').doc(reviewBody.restaurantId).collection('reviews').doc(reviewBody.id).set(reviewBody);
    response.send({ status : 200, message : "Restaurant Review Added"})
  });
});

/*******************************************
************* UPDATE METHODS ***************
*******************************************/

exports.updateRestaurant = functions.https.onRequest((request, response) => {
  updateRestaurant(request.body);
  response.send({ status : 200, message : "Restaurant Updated. ID: " + request.body.id})
});

exports.updateUser = functions.https.onRequest((request, response) => {
  var userBody = request.body;

  if(userBody.id === null || userBody.id === "" || userBody.id === undefined){
      response.send({ status : 406, message : "406 Not Acceptable. User ID missing or empty."});
      return;
  }

  updateUser(request.body);

  response.send({ status : 200, message : "User Updated. ID: " + request.body.id})
});

exports.createClientUser = functions.https.onRequest((request, response) => {
  var user = request.body


  return createClientUser(user)
    .then(clientObject => {
      return response.send({
        status : 200,
        message : `Client ${clientObject.id} was created on firebase`,
        client : clientObject
      })
    })
    .catch(error => {
      return response.send({
        status : 400,
        message : "400 Bad Request: Unable to create new client ",
        error : error
      })
    })
  })

/*******************************************
************ BRAINTREE METHODS *************
*******************************************/

exports.createNewCustomerTrigger = functions.firestore.document("users/{userId}").onCreate(userSnap => {
  let user = userSnap.data()
  let firstName = user.firstName
  let lastName = user.lastName
  let clientId = userSnap.id


  // check if valid request
  if (isEmpty(clientId)) {
    return console.error({
      status : 400,
      message : "400 Bad Request: Client id missing."
    })
  }
  let customer = {}

  if (isEmpty(firstName)) {
    return console.error({
      status : 400,
      message : "400 Bad Request: First name is missing"
    })
  } else { customer.firstName = firstName }
  if (isEmpty(lastName)) {
    return console.error({
      status : 400,
      message : "400 Bad Request: Last name is missing"
    })
  } else { customer.lastName = lastName }

  // create BrainTree customer
  return createBTCustomer(customer)
  .then(customerBT => {
    console.log("writing to brain tree")
    return firestore.collection('braintree').doc(clientId).set({id : customerBT.id})
    .then(result => {
      console.log(result)
      return result
    })
    .catch(error => {
      console.error(error)
      throw error
    })
  })
  .then(firestoreResult => {
    console.log("Sending Response")
    return console.log({
      status : 200,
      message : "Success! New customer was created."
    })
  })
  .catch(err => {
    console.error(err)
    return console.error({
      status : err.status,
      message : "400 Bad Request: Could not create new customer",
      error : err
    })
  })
})

exports.createNewCustomer = functions.https.onRequest((request, response) => {
  let firstName = request.body.firstName
  let lastName = request.body.lastName
  let clientId = request.body.clientId

  // check if valid request
  if (isEmpty(clientId)) {
    return response.send({
      status : 400,
      message : "400 Bad Request: Client id missing."
    })
  }
  if (isEmpty(firstName)) {
    return response.send({
      status : 400,
      message : "400 Bad Request: First name is missing"
    })
  }
  if (isEmpty(lastName)) {
    return response.send({
      status : 400,
      message : "400 Bad Request: Last name is missing"
    })
  }

  let customer =  {
    firstName : firstName,
    lastName : lastName
  }

  // create BrainTree customer
  return createBTCustomer(customer)
  .then(customerBT => {
    console.log("writing to brain tree")
    return firestore.collection('braintree').doc(clientId).set({id : customerBT.id})
    .then(result => {
      console.log(result)
      return result
    })
    .catch(error => {
      console.error(error)
      throw error
    })
  })
  .then(firestoreResult => {
    console.log("Sending Response")
    return response.send({
      status : 200,
      message : "Success! New customer was created."
    })
  })
  .catch(err => {
    console.error(err)
    return response.send({
      status : err.status,
      message : "400 Bad Request: Could not create new customer",
      error : err
    })
  })
})

exports.generateBTClientToken = functions.https.onRequest((request, response) => {
  let clientId = request.body.clientId

  if (isEmpty(clientId)) {
    // Invalid Request
    return response.send({
      status : 400,
      message : "400 Bad Request: Client Id was not found.",
      error : "Client Id was nil"
    })
  }
  // Get Customer Id
  return firestore.collection('braintree').doc(clientId).get()
  .then(customerSnap => {
    let customer = customerSnap.data()
    let customerId = customer.id
    return gateway.clientToken.generate( {customerId : customerId}, function (err, res) {
      var clientToken = res.clientToken
      if (!isNull(clientId)) {
        // send back client token
        return response.send( {
          status : 200,
          message : "Creation of client token was successful!",
          clientToken : clientToken
        })
      } else {
        // no client token recieved
        return response.send({
          status : 500,
          message : "Unable to create client token.",
          error : err
        })
      }
    })
  })
})

// update braintree customer data when firestore updates
exports.updateBTCustomerInfomation = functions.firestore.document('users/{userId}').onCreate(userSnap => {
  let user = userSnap.data()
})

exports.testBTFindCustomer = functions.https.onRequest((req, res)  => {
  let customerId = req.body.customerId

  if (isEmpty(customerId)) {
    return new functions.https.HttpsError('failed-precondition', 'check customerId field', "where the fuck is this going");
  }

  return findBTCustomer(customerId)
  .then(customer => {
    console.log("FOUND CUSTOMER")

      return res.send({
        status : 200,
        customer : customer,
        message : "Success! New customer was created."
      })
  })
  .catch(err => {
    console.log("NO CUSTOMER")
    console.error(err)
    return res.send({
      status : err.status,
      message : "400 Bad Request: Could not find braintree customer",
      error : err
    })
  })

})

// create braintree customer
function createBTCustomer(customer) {
  return new Promise((resolve,reject) => {
    gateway.customer.create({
      firstName: customer.firstName,
      lastName: customer.lastName,
    }, function (err, result) {
      if (result.success) {
        // return new customer object
        console.log(result.customer)
        resolve(result.customer)
      } else {
        console.error(err)
        reject(err)
      }
    })
  })
}

// create braintree customer
function findBTCustomer(customerId) {
  return new Promise((resolve, reject) => {
    gateway.customer
    .find(customerId, function (err, customer) {
      if (isNull(err)) { resolve(customer) }
      else { reject(err) }
    });
  })
}

function addPaymentMethod(customerId, nonceFromClient) {
  if (isEmpty(customerId)) {
    throw "Invalid Call: No customer Id"
  }
  if (isNull(nonceFromClient)) {
    throw "Invalid Call: No Payment nonce"
  }

  // add payment nonce from client
  return gateway.paymentMethod.create({
    customerId: customerId,
    paymentMethodNonce: nonceFromTheClient
  }, function (err, result){

   });
}

/*******************************************
************ PRIVATE METHODS ***************
*******************************************/


function updateRestaurant(data){
  var setDoc = firestore.collection('restaurants').doc(data.id).set(data);
}

function updateCategory(data){
  var setDoc = firestore.collection('restaurants').doc(data.restaurantId).collection('menu').doc(data.id).set(data);
}

function updateMenuItem(data){
  var setDoc = firestore.collection('restaurants').doc(data.restaurantId).collection('menu').doc(data.categoryId).collection('items').doc(data.id).set(data);
}

function updateUser(data){
  var setDoc = firestore.collection('users').doc(data.id).set(data);
}

/**
 * @function updateClientUser
 *  Update existing client object on firestore
 * @param {String} clientId
 *  Id of client to be updated
 * @param {Object} update
 *  object used to update the fileds under client entry
 *
 * @return {Promise}
 *  returns Promise of the update
 */
function updateClientUser(clientId, update) {
  return firestore.doc(`users/${clientId}`).update(update)
}

/**
 * @function createClientUser
 *  Creates client user object on firestore
 * @param clientObject
 *  Client Object to be written into firestore
 *
 * @return {Promise}
 *  Promise that resloves with client object and and throws with FirestoreWriteError
 */
function createClientUser(clientObject) {
  return firestore.collection("users").add(clientObject)
  .then(clientDocRef => {
    clientObject.id = clientDocRef.id
    return clientDocRef.update({id : clientObject.id})
  })
  .then(result => {
    return clientObject
  })
}

function timeStamp() {
  return new Date().getTime()
}

function isNull(a) { return a === null || a === undefined }

function isEmpty(a) { return a === "" || isNull(a) }

/**
 *
 * @param {String} restaurantId
 *  Id of request restaurant
 *
 * @returns {Object} restaurant
 *  returns entire restaurant object
 */
function getRestaurant(restaurantId) {
  return new Promise((resolve, reject) => {
    firestore.collection('restaurants').doc(restaurantId).get()
    .then(docSnap => {
      return getEntireDocument(docSnap)
    })
    .then(doc => {
      resolve(doc)
    })
    .catch(error => {
      reject(error)
    })
  })
}

// returns entire doc, including subcollections as an object
function getEntireDocument(docSnap) {
  return new Promise((resolve, reject) => {
    // get current Document Data
    var doc = docSnap.data()

    // get subcollections
    docSnap.ref.getCollections()
    .then(collectionsRef => {

      // query subcolletions
      var getSubCollections = []

      // for each subcollection make query for subdocuments
      collectionsRef.forEach(collectionRef => {
        getSubCollections.push(collectionRef.get()
        .then(collectionSnap => {
          var collection = {}
          var getSubDocuments = []

          collectionSnap.docs.forEach(subDocSnap => {
            //recursion
            getSubDocuments.push(getEntireDocument(subDocSnap)
            .then(result => {
              collection[subDocSnap.id] = result
              return subDocSnap.id
            }))
          })

          // return collection after all getting subDoc
          return Promise.all(getSubDocuments)
          .then(results => {
            return collection
          })
        })
        .then(result => {
          // add collection to doc after return
          doc[collectionRef.id] = result
          return collectionRef.id
        }))
      })
      // return after adding all subcollections
      return Promise.all(getSubCollections)
      .then(results => {
        return doc
      })
    })
    .then(result => {
      resolve(doc)
      console.log(doc)
    })
  })
}

function updateSession(session){
    let timestamp = timeStamp();
    session.updatedAt = timestamp;
    firebase.ref('/order_sessions/' + session.id).set(session);
    firestore.collection('sessions').doc(session.id).set(session);
}
